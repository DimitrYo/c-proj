#ifndef CLIENTTANK_H
#define CLIENTTANK_H
#include <QObject>
#include <QTcpSocket>
#include <QWidget>
#include <QMessageBox>
//#include "servertank.h"
#include <QAbstractSocket>

#include "tank_player.h"
class clienttank : QObject
{
    Q_OBJECT
    QString Host;
    quint16 Port;
    int client_num;
    QVector<tank_player*> *players;
private slots:
    void onSokConnected();
    void onSokDisconnected();
    void onSokReadyRead();
    void onSokDisplayError(QAbstractSocket::SocketError socketError);

signals:

public:

    clienttank(QString _host , quint16 _port,QObject *parent = 0);

    static const QString constNameUnknown;
    static const quint8 comAutchReq = 1;
    static const quint8 comUserJoin = 2;
    static const quint8 comUserLeft = 3;
    //explicit clienttank(const clienttank& ct);

    ~clienttank();

    void doSendCommand(quint8 comm) const;
    void connecttoHost();
    void disconectfromHost();
    void setPlayers(QVector<tank_player*> *t);





private:
    QTcpSocket *_sok;
    quint16 _blockSize;

};

#endif // CLIENTTANK_H

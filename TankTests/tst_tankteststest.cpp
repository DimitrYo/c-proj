#include <QString>
#include <QtTest>

#include "../tank.h"


class TankTestsTest : public QObject
{
    Q_OBJECT

public:
    TankTestsTest();

private Q_SLOTS:
    void testCase1_data();
    void testCase1();
};

TankTestsTest::TankTestsTest()
{
}

void TankTestsTest::testCase1_data()
{
    QTest::addColumn<int>("first");
    QTest::addColumn<int>("expected");

    QTest::newRow("compare01") << 0 << 0 ;
    QTest::newRow("compare02") << 1 << 1 ;
    QTest::newRow("compare03") << 2 << 2 ;
    QTest::newRow("compare04") << 3 << 3 ;
}
void TankTestsTest::testCase1()
{


    QFETCH(int, first);
    QFETCH(int, expected);


    tank t;
    t.SetDirection(first);
    int actual = t.GetDirection();
    QCOMPARE(actual, expected);
}

QTEST_APPLESS_MAIN(TankTestsTest)

#include "tst_tankteststest.moc"

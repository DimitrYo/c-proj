QT += testlib declarative core gui network


greaterThan(QT_MAJOR_VERSION, 4): QT += printsupport

TARGET = tst_tankteststest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app



SOURCES += tst_tankteststest.cpp \
            ../tank.cpp \
            ../bullet.cpp \
            ../explosion.cpp
#HEADERS  += tank.h

#INCLUDEPATH += ../
#DEPENDPATH += $${INCLUDEPATH} # force rebuild if the headers change

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>
#include <QVector>
#include <QTimer>
//MY
#include "drawwidget.h"
#include "infowidget.h"
#include "tank_player.h"
#include "clienttank.h"
#include "bullet.h"
#include "controlwidget.h"
#include <QFile>
#include <QSound>
#include "botanalyzing.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT


    // GAME
    bool gameisstart;
    bool game_win;
    bool gamerisplayed ;
    bool gameispause;
    quint8 num_players;
    // MODE Client Editor
    quint8 mode;
    quint8 level;

    //data
    QVector<tank_player*> *players;
    QVector<player*> controlplayer;
    QVector<tank*> *enemies;

    QVector<bullet*> *bullets;
    QVector<explosion*> *explosions;

    QVector<mymap*> maps;
    mymap *editor;

    // client-server
    QString Host;
    quint16 Port;
    //clienttank *_client;

    // Timer
    QTimer *timer;

    // Keyboard
    //QVector<players> keysPressed;


    QSound *soundshoot;
    int counter;
    int counter_sec;
    int num_enemy;


    //Bot
    botanalyzing botan;
    int timelevel;
public slots:
    void newgame();
    void changemode();
    void changemodetoClient();
    void changemodetoServer();
    void changemodetoEditor();
    void setting();

    void gamerunning();
    void checkbullet();
    void checkplayers();

    void soundplayon(int);

signals:
    void changedmode(int);
    void gamestart(bool);
    void soundplay(int);
    void taimerinfo(int);
    void gameonpause(bool);
public:
    explicit MainWindow(QWidget *parent = 0);

    // Elements on Widget
    QAction *actionNew;
    QAction *actionmLevel;
    QAction *actionSaveLevel;
    QAction *actionExit;
    QAction *actionPause;
    QAction *actionClient;
    QAction *actionEditor;
    QWidget *centralWidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;

    //Draw Widgets
    DrawWidget *DrawArea;
    InfoWidget *InfoArea;

    // Menu
    QMenuBar *menuBar;
    QMenu *menuExit;
    QMenu *menuMode;
    QMenu *menuSetting;
    QMenu *menuHelp;
    QToolBar *mainToolBar;


    // editor, client ,server

//MY
    void retranslateUi();
    void makeGui();
    void connectsignals();
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);

    bool loadmaps();
    bool savemap();
    void cleanmas();
    bool loadsettings();
    bool savesettings();
    void loadsound();

    ~MainWindow();

};

#endif // MAINWINDOW_H

#include "bullet.h"
#include "controlwidget.h"

bullet::bullet()
{
    type = 2;
    speed = 5;
}

void bullet::SetSpeed(int s) {
    speed = s;
}

void bullet::SetType(int t) {
    type = t;
}

void bullet::SetCenter(QPoint c) {
    center = c;
}

void bullet::SetDir(int d) {
    dir = d;
}

int bullet::GetDir() {
    return dir;
}

int bullet::GetSpeed() {
    return speed;
}

int bullet::GetType() {
    return type;
}

QPoint bullet::GetCenter() {
    return center;
}

explosion* bullet::getExpl() {
    explosion* temp = new explosion();
    temp->SetCenter(this->GetCenter());
    return temp;
}

QRect bullet::GetBounds() {
    return QRect(center.x() - BALLSIZE/2 , center.y() -BALLSIZE/2 , BALLSIZE,BALLSIZE);
}

void convertbricks(int type , int x,int y , mymap * checkmap) {
    switch(type) {
    case 1: {
        switch(checkmap->area[x][y]) {
        case 1:{
            checkmap->area[x][y] = 20;
        }
        case 20:{
            checkmap->area[x][y] = 16;
        }
        case 16:{
            checkmap->area[x][y] = 14;
        }
        case 14:{
            checkmap->area[x][y] = 3;
        }
        }
    }
    case 2: {
        switch(checkmap->area[x][y]) {
        case 1:{
            checkmap->area[x][y] = 19;
        }
        case 19:{
            checkmap->area[x][y] = 11;
        }
        case 11:{
            checkmap->area[x][y] = 7;
        }
        case 7:{
            checkmap->area[x][y] = 3;
        }
        }
    }/*
    case 3: {
        switch(checkmap->area[x][y]) {
        case 1:{

            checkmap->area[x][y] = 20;
        }
        case 20:{
            checkmap->area[x][y] = 16;
        }
        case 16:{
            checkmap->area[x][y] = 14;
        }
        case 14:{
            checkmap->area[x][y] = 3;
        }
        }
    }
    case 4: {
        switch(checkmap->area[x][y]) {
        case 1:{
            checkmap->area[x][y] = 20;
        }
        case 20:{
            checkmap->area[x][y] = 16;
        }
        case 16:{
            checkmap->area[x][y] = 14;
        }
        case 14:{
            checkmap->area[x][y] = 3;
        }
        }
    }*/
    }
}

bool checkMoveUP(int x,int y, mymap * checkmap) {
    int _x = x;
    int _y = y;
    //    qDebug() << y;
    x = x / TILESIZE;
    y = y / TILESIZE;

    if ( _y % TILESIZE == 0)
        if (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6) {
            convertbricks(1, _x,_y , checkmap);
            return false;
        }
    x = (_x - (BALLSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   ){
        convertbricks(1, _x,_y , checkmap);
        return false;
    }

    x = (_x + (BALLSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
    {
        convertbricks(1, _x,_y , checkmap);
        return false;
    }
    return true;
}

bool checkMoveDown(int x,int y, mymap * checkmap) {

    int _x = x;
    int _y = y;
    //    qDebug() << y;
    //int x2 = x ;
    y = y / TILESIZE;
    x = x / TILESIZE;
    if ( _y % TILESIZE == 0)
        if (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)
        {
            convertbricks(3, _x,_y , checkmap);
            return false;
        }
    x = (_x - (BALLSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
    {
        convertbricks(3, _x,_y , checkmap);
        return false;
    }

    x = (_x + (BALLSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
    {
        convertbricks(3, _x,_y , checkmap);
        return false;
    }
    return true;

}

bool checkMoveRight(int x,int y, mymap * checkmap) {
    int _x = x;
    int _y = y;
    //    qDebug() << x;
    x = x / TILESIZE;
    y= y / TILESIZE;

    if ( _x % TILESIZE == 0)
        if (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)
        {
            convertbricks(2, _x,_y , checkmap);
            return false;
        }

    y = (_y - (BALLSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
    {
        convertbricks(2, _x,_y , checkmap);
        return false;
    }

    y = (_y + (BALLSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
    {
        convertbricks(2, _x,_y , checkmap);
        return false;
    }
    return true;
}

bool checkMoveLeft(int x,int y, mymap * checkmap) {
    int _x = x;
    int _y = y;
    //    qDebug() << x;
    x = x / TILESIZE;
    y= y / TILESIZE;

    if ( _x % TILESIZE == 0)
        if (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)
        {
            convertbricks(4, _x,_y , checkmap);
            return false;
        }


    y = (_y - (BALLSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
        // if ( (_y < (y * TILESIZE)) )
    {
        convertbricks(4, _x,_y , checkmap);
        return false;
    }

    y = (_y + (BALLSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
        // if ( (_y < (y * TILESIZE)) )
    {
        convertbricks(4, _x,_y , checkmap);
        return false;
    }
    return true;
}

bool bullet::move(mymap * checkmap) {
    switch(dir){    case 1: {
        int temp = center.y()- speed ;
        //        qDebug() << temp << temp / TILESIZE << (temp / TILESIZE)*TILESIZE<< checkMoveUP(center.x(),temp- (BALLSIZE/2),checkmap);
        if (temp > 0 && checkMoveUP(center.x(),(temp-(BALLSIZE/2)),checkmap))
            center.setY(temp);
        else return false;
    }break;
    case 2: {

        int temp =center.x()+ speed ;
        //       qDebug() << temp << temp / TILESIZE << (temp / TILESIZE)*TILESIZE<< checkMoveRight(temp+ (BALLSIZE/2),center.y(),checkmap);
        if (temp < 640 && checkMoveRight(temp+ (BALLSIZE/2),center.y(),checkmap))
            center.setX(temp);
        else return false;
    }break;
    case 3: {

        int temp = center.y()+speed;
        //        qDebug() << temp << temp / TILESIZE << (temp / TILESIZE)*TILESIZE<< checkMoveDown(center.x(),temp + (BALLSIZE/2),checkmap);
        if (temp < 640 && checkMoveDown(center.x(),temp + (BALLSIZE/2),checkmap))
            center.setY(temp);
        else return false;
    }break;
    case 4: {

        int temp = center.x()-speed;
        //        qDebug() << temp << temp / TILESIZE << (temp / TILESIZE)*TILESIZE<<  checkMoveLeft(temp - (BALLSIZE/2),center.y(),checkmap);
        if ( temp > 0 && checkMoveLeft(temp-(BALLSIZE/2) ,center.y(),checkmap))
            center.setX(temp);
        else return false;
    }break;
    }
    return true;
}

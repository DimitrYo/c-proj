#ifndef BULLET_H
#define BULLET_H
#include <QPoint>
#include "controlwidget.h"
#include "explosion.h"
class bullet
{
    int speed;
    int type;
    QPoint center;
    int dir;
public:
    bullet();
    void SetSpeed(int s);
    void SetType(int t);
    void SetCenter(QPoint c);
    void SetDir(int _dir);
    int GetDir();
    int GetSpeed();
    int GetType();
    QPoint GetCenter();
    bool move(mymap * checkmap);
    explosion* getExpl();
    QRect GetBounds();
};

#endif // BULLET_H

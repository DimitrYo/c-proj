#ifndef TANK_H
#define TANK_H
#include <QPoint>
#include <QRect>
#include <QImage>

#include "controlwidget.h"
#include "bullet.h"

struct tank_mes {
    int health;
    int speed;
    int direction;
    QPoint center;
    int type;
};
/**
 * @brief Tank class implement the object tank in game
 */
class tank
{
protected:
    /**
     * @brief Structure for Client-Server send-receivee data
     */
    tank_mes data;
public:
    /**
     * @brief Usual Constructor
     */
    tank();

    /**
     * @brief Copy Constructor
     */
    tank(const tank& );

    /**
     * @brief Function that set the direction of tank
     * @param dir - direction
     * 0 - up
     * 1 - down
     * 2 - left
     * 3 - rigth
     */
    void SetDirection(int dir);

    /**
     * @brief Function that set the speed of tank
     * @param sp - speed
     */
    void SetSpeed(int sp);

    /**
     * @brief Function that set the Health of tank
     * @param h - health of tank
     */
    void SetHealth(int h);

    /**
     * @brief Function that set the center point of tank
     * @param c - center QPoint
     */
    void SetCenter(QPoint c);
    /**
     * @brief Function that set the Type of tank
     * @param t - type
     * 1 - my
     * 2 - enemy
     */
    void SetType(int t);

    /**
     * @brief Function that get the direction of tank
     * @return direction
     */
    int GetDirection();

    /**
     * @brief GetSpeed
     * @return
     */
    int GetSpeed();

    /**
     * @brief GetHealth
     * @return
     */
    int GetHealth();

    /**
     * @brief GEtCenter
     * @return
     */
    QPoint GEtCenter();

    /**
     * @brief GetType
     * @return
     */
    int GetType();

    /**
     * @brief GetData
     * @return
     */
    tank_mes GetData();

    /**
     * @brief Calculate the bounds and return it's
     * @return QRect of bounds
     */
    QRect GetBounds();

    /**
     * @brief isdead - life or dead tank
     */
    bool isdead;

    /**
     * @brief move - changing the position in the map
     * @param dir - move in this direction
     * @param checkmap - map or checking
     */
    void move(int dir,mymap * checkmap);

    /**
     * @brief shoot - return the new bullet that tank shoot
     * @return bullet
     */
    bullet* shoot();

    /**
     * @brief getIsdead
     * @return
     */
    bool getIsdead() const;

    /**
     * @brief setIsdead
     * @param value
     */
    void setIsdead(bool value);
};

#endif // TANK_H

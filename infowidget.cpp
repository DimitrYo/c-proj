#include "infowidget.h"
#include <QPainter>
#include <QImage>
#include <QDebug>
#include <QMouseEvent>
InfoWidget::InfoWidget(QWidget *parent) :
    QWidget(parent)
{
    QString str;
    for (quint8 i = 0; i < BLOCKSCOUNT; i++) {
        str = BLOCKSPATH + QString("%1").arg(i)+".png";
        blocks[i].load(str.toStdString().c_str(),"PNG");
    }
}

void InfoWidget::paintEvent(QPaintEvent *) {
    QPainter painter(this);
    painter.fillRect(0,0,120,640,QColor(80,80,80,255));

    if ( mode == 2) { // editor
        drawarea(painter);
    } else
        if ( mode == 1) { // client

    }


}

void InfoWidget::drawarea(QPainter &painter) {/*
    for(quint32 i = 0; i < AREASIZE; i++) {
        for(quint32 j = 0; j < AREASIZE; j++) {
            QRectF target1(i*TILESIZE, j*TILESIZE, TILESIZE, TILESIZE);
            painter.drawImage(target1, tiles[drawmap->area[i][j]]);
        }
    }*/

    for(quint32 i = 0; i < BLOCKSCOUNT; i++) {
        QRectF target1((i/10)*TILESIZE*3, (i%10)*TILESIZE*3, TILESIZE*3, TILESIZE*3);
        painter.drawImage(target1, blocks[i]);
    }
}

void InfoWidget::mousePressEvent(QMouseEvent *event) {
    int i = event->x() / (TILESIZE*3);
    int j = event->y() / (TILESIZE*3);
    qDebug() << 10*i+j <<  event->x() << event->y();
    if ( (10*i+j) < BLOCKSCOUNT && (10*i+j) >=0)
    emit numblock(10*i+j);


}

void InfoWidget::areachangedmode(int _m) {
    mode = _m;
    update();
}

void InfoWidget::taimerinfoslot(int t) {
    taimertext = QString("%1").arg(t);
}


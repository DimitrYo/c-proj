#ifndef BOTANALYZING_H
#define BOTANALYZING_H
#include "tank_player.h"
#include "controlwidget.h"
#include <QPoint>
class botanalyzing
{
    int num_players;
    QPoint resp1;
    QPoint resp2;
    QVector<tank_player *> *p;

public:
    botanalyzing();
    tank *create();
    void analyze(mymap * m, QVector<tank_player*> *pl);
    void command(QVector<tank*> *en);

};

#endif // BOTANALYZING_H

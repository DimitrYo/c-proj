#ifndef INFOWIDGET_H
#define INFOWIDGET_H

#include <QWidget>
#include "controlwidget.h"

#define BLOCKSCOUNT 17

class InfoWidget : public QWidget
{
    Q_OBJECT
    quint8 mode;
    QString taimertext;
    QImage blocks[BLOCKSCOUNT];
    mymap editor;
public slots:
    void areachangedmode(int);
    void taimerinfoslot(int);
public:
    explicit InfoWidget(QWidget *parent = 0);
    void paintEvent(QPaintEvent *);
    void drawarea(QPainter& painter);
    void mousePressEvent(QMouseEvent *);
signals:
    void numblock(int);
        
};

#endif // INFOWIDGET_H

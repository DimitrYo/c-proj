#ifndef DRAWWIDGET_H
#define DRAWWIDGET_H

#include <QWidget>
#include <QStyle>
#include <QPainter>
#include <QVector>

//MY
#include "tank.h"
#include "tank_player.h"
#include "bullet.h"
#include "controlwidget.h"
#include "explosion.h"


class DrawWidget : public QWidget
{
    Q_OBJECT
    QImage tiles[IMAGECOUNT];
    QImage players_image[4][4];
    QImage enemy_image[4][4];
    QImage blocks[BLOCKSCOUNT];
    QImage explosions[EXPLCOUNT];
    QImage ball;
    QImage comet[4];
    mymap *drawmap;
    quint8 editor[AREASIZE/2][AREASIZE/2];


    QVector<tank_player*> *players;
    QVector<tank*> *enemies;
    QVector<bullet*> *bullets;
    QVector<explosion*> *expl;
    int mode;
    int typedrawblock;
    int drawx;
    int drawy;
    bool  drawblock;
    bool gameisstart;
    bool notdrawed;
    bool gameispause;
public slots:
    void areachangedmode(int);
    void gamestart(bool);
    void getblock(int);
    void gameispauseslot(bool);
public:
    explicit DrawWidget(QWidget *parent = 0);
    void paintEvent(QPaintEvent *);
    void drawarea(QPainter& painter);
    void drawareablocks(QPainter& painter);
    void drawplayers(QPainter& painter);
    void drawenemies(QPainter& painter);
    void drawbullets(QPainter& painter);
    void drawexplosion(QPainter& painter);
    void drawtrees(QPainter& painter);

    void setmap(mymap* rawmymap);
    void loadimages();
    void loadlevels();
    bool savelevel(QString);
    void setPlayers(QVector<tank_player*> *t);
    void setBullets(QVector<bullet*> *b);
    void setExplosions(QVector<explosion*> *e);
    void setEnemy(QVector<tank*> *b);
    void mousePressEvent(QMouseEvent *);
    QImage getBlocks() ;
signals:
    
public slots:
    
};

#endif // DRAWWIDGET_H

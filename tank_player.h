#ifndef TANK_PLAYER_H
#define TANK_PLAYER_H
#include "tank.h"
class tank_player : public tank
{
public:
    tank_player();
    //explicit tank_player( const tank& t);
    explicit tank_player( const tank_player& t);
};

#endif // TANK_PLAYER_H

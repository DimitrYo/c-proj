#ifndef CONTROLWIDGET_H
#define CONTROLWIDGET_H

#define BLOCK_WATER 0
#define BLOCK_TREE 4
#define BLOCK_WALL 1
#define BLOCK_WALLD 2
#define BLOCK_ICE 5
#define BLOCK_BASE 16
#define BLOCK_EMPTY 3

#define MAPSCOUNT 11
#define BLOCKSCOUNT 17
#define IMAGECOUNT 20
#define EXPLCOUNT 14

#define TILESIZE 16
#define  TANKSIZE 24
#define BALLSIZE 7

#define AREAPIXELS 640
#define AREASIZE  (AREAPIXELS/TILESIZE)

#define TILESPATH ":/tiles/images/tiles/tile"
#define TANKSSPATH ":/tanks/images/tanks/player"
#define BLOCKSPATH ":/blocks/images/blocks/block_"
#define ENEMYSSPATH ":/tanks/images/tanks/enemy"
#define BULLETSSSPATH ":/bullet/images/balls/comet"
#define MAPSPATH ":/maps/maps/map_"
#define EXPLPATH ":explosions/images/explosions/explosion"

#define BULLETPATH ":bullets/images/balls/Ball"
#define COMETPATH ":bullets/images/balls/comet"

#include <QVector>
#include <QPoint>
#include <QRect>
#include <QDebug>
class mymap {
public:
    mymap() {}
    mymap(const mymap& m) {
        qCopy(m.enemy_respaun.begin(),m.enemy_respaun.end(),enemy_respaun.begin());
        qCopy(m.player_respaun.begin(),m.player_respaun.end(),player_respaun.begin());
        memcpy(area,m.area,sizeof(quint8)*AREASIZE*AREASIZE);
        base_pos = m.base_pos;
                      }
    ~mymap() {enemy_respaun.clear();player_respaun.clear();}
    quint8 area[AREASIZE][AREASIZE];
    QPoint base_pos;
    QVector<QPoint> enemy_respaun;
    QVector<QPoint> player_respaun;
    void set(int i,int k) {
        int j = i/20;
        i = i%20;
        area[i*2][j*2] = k;
        area[2*i+1][2*j+1] = k;
        area[2*i][2*j+1] = k;
        area[2*i+1][2*j] = k;
    }
    void setbase(int i) {
        set(i,3);
        base_pos = QPoint((i%20)*2*TILESIZE,(i/20)*2*TILESIZE);

    }
};
struct player {
public:
    player() { shoot = 45; isshoot = false; player_direction =-1 ; }
    int shoot;
    bool isshoot;
    qint8 player_direction;
};


#endif // CONTROLWIDGET_H

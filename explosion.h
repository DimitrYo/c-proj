#ifndef EXPLOSION_H
#define EXPLOSION_H
#include <QPoint>
class explosion
{
    QPoint center;
    int time;
public:
    explosion();
    void SetCenter(QPoint c);
    void SetTime(int _dir);
    QPoint GetCenter();
    int GetTime();
};

#endif // EXPLOSION_H

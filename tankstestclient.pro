#-------------------------------------------------
#
# Project created by QtCreator 2013-07-21T15:11:13
#
#-------------------------------------------------

QT       += core gui network testlib gui



TARGET = tankstestclient
TEMPLATE = app
SUBDIRS += TankTest

SOURCES += main.cpp\
        mainwindow.cpp \
    tank.cpp \
    drawwidget.cpp \
    infowidget.cpp \
    tank_player.cpp \
    bullet.cpp \
    clienttank.cpp \
    controlwidget.cpp \
    explosion.cpp \
    botanalyzing.cpp

HEADERS  += mainwindow.h \
    tank.h \
    drawwidget.h \
    infowidget.h \
    tank_player.h \
    bullet.h \
    clienttank.h \
    controlwidget.h \
    explosion.h \
    botanalyzing.h

FORMS    +=

RESOURCES += \
    res.qrc


#include <QFile>
#include <QTextStream>
#include <QResource>
#include <QByteArray>
#include "controlwidget.h"
#include "mainwindow.h"

bool MainWindow::loadmaps() {
    for( quint8 i = 1; i <= MAPSCOUNT; i++) {
        QResource res(MAPSPATH + QString("%1.txt").arg(i));
        QByteArray data;//(*(res.data()) ,res.size());
        //data << *(res.data());

        mymap *loadedmap = new mymap();
        memset(loadedmap->area,BLOCK_EMPTY,sizeof(quint8)*AREASIZE*AREASIZE);
        data = qUncompress(res.data(),res.size());
        quint32 size = data.size();
        int k = 0;
       // qDebug() << data;
        for( quint32 j = 0; j < size; j++) {
            switch(data[j]) {
            case '0': loadedmap->set(k,BLOCK_EMPTY); ; break;
            case '1': loadedmap->set(k,BLOCK_WALLD) ; break;
            case '2': loadedmap->set(k,BLOCK_WALL); ;break;
            case '3': loadedmap->set(k,BLOCK_WALL);   ;break;
            case '4': loadedmap->set(k,BLOCK_WALL);  ;break;
            case '5': loadedmap->set(k,BLOCK_WALL);   ;break;
            case '6': loadedmap->setbase(k); ;break;
            case '7': loadedmap->set(k,BLOCK_WATER);  ;break;
            case '8': loadedmap->set(k,BLOCK_TREE); ;break;
            case '9': loadedmap->set(k,BLOCK_ICE);  ;break;
            default: k--; break;

            }
            ++k;
        }
        maps.push_back(loadedmap);
    }
    return true;
}


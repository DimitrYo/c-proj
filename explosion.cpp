#include "explosion.h"

explosion::explosion()
{
    time = 0;
}

void explosion::SetTime(int t) {
    time = t;
}

void explosion::SetCenter(QPoint c) {
    center = c;
}

int explosion::GetTime() {
    return time;
}

QPoint explosion::GetCenter() {
    return center;
}

#include "clienttank.h"
#include <iostream>

using namespace std;

const QString clienttank::constNameUnknown = QString(".Unknown");

clienttank::clienttank(QString _host, quint16 _port,QObject *parent):QObject(parent) {
    Host = _host;
    Port = _port;
    //храниим указатель на объект-сервер
    //_serv = serv;
    //клиент не прошел авторизацию
    //_isAutched = false;
    //_name = constNameUnknown;
    //размер принимаемого блока 0
    _blockSize = 0;
    //создаем сокет
    _sok = new QTcpSocket(this);
    //connecttoHost();
    //устанавливаем дескриптор из incomingConnection()
    //_sok->setSocketDescriptor(desc);
    //подключаем сигналы

    QObject::connect(_sok, SIGNAL(connected()),this,SLOT(onSokConnected()));
    QObject::connect(_sok,SIGNAL(disconnected()),this,SLOT(onSokDisconnected()));
    QObject::connect(_sok, SIGNAL(readyRead()), this, SLOT(onSokReadyRead()));
    QObject::connect(_sok,SIGNAL(error(QAbstractSocket::SocketError)),SLOT(onSokDisplayError(QAbstractSocket::SocketError)));
}
/*
clienttank::clienttank(const clienttank &ct) {
    this->_sok = ct._sok;
    Host = ct.Host;
    Port = ct.Port;
}*/


clienttank::~clienttank()
{
    delete _sok;
}

void clienttank::onSokConnected()
{
    cout << "Client connected" << endl;
    //never calls, socket already connected to the tcpserver
    //we just binding to this socket here: _sok->setSocketDescriptor(desc);
}

void clienttank::onSokDisconnected()
{
    qDebug() << "Client disconnected";
    //если авторизован

    deleteLater();
}

void clienttank::onSokDisplayError(QAbstractSocket::SocketError socketError)
{
    //w нужна для обсвобождения памяти от QMessageBox (посредством *parent = &w)
    QWidget w;
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(&w, "Error", "The host was not found");
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(&w, "Error", "The connection was refused by the peer.");
        break;
    default:
        QMessageBox::information(&w, "Error", "The following error occurred: "+_sok->errorString());
    }
    //тут вызовутся деструктор w и соответственно QMessageBox (по правилам с++)
}

void clienttank::onSokReadyRead()
{
    QDataStream in(_sok);
    //если считываем новый блок первые 2 байта это его размер
    if (_blockSize == 0) {
        //если пришло меньше 2 байт ждем пока будет 2 байта
        if (_sok->bytesAvailable() < (int)sizeof(quint16))
            return;
        //считываем размер (2 байта)
        in >> _blockSize;
        qDebug() << "_blockSize now " << _blockSize;
    }
    //ждем пока блок прийдет полностью
    if (_sok->bytesAvailable() < _blockSize)
        return;
    else
        //можно принимать новый блок
        _blockSize = 0;
    //3 байт - команда серверу
    quint8 command;
    in >> command;
    qDebug() << "Received command " << command;
    switch(command)
    {
    case comAutchReq: {

    }break;
    case comUserJoin: {

    }break;
    case comUserLeft: {

    }break;
    }
}

void clienttank::doSendCommand(quint8 comm) const
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out << (quint16)0;
    out << comm;
    out.device()->seek(0);
    out << (quint16)(block.size() - sizeof(quint16));
    _sok->write(block);
    //qDebug() << "Send to" << _name << "command:" << comm;
}
void clienttank::connecttoHost() {
    _sok->connectToHost(Host, Port);
    cout <<  QString("Error %1").arg(_sok->error()).toStdString();
    doSendCommand(1);
}
void clienttank::disconectfromHost() {
    _sok->disconnectFromHost();
}

void clienttank::setPlayers(QVector<tank_player*> *t) {
    players = t;
}




#include "tank.h"
#include <QDebug>
tank::tank()
{
    data.health = 200;
    data.direction = 1;
    data.speed = 3;
    isdead = false;
}

tank::tank(const tank &t) {
    data = t.data;
}

void tank::SetDirection(int dir) {
    data.direction = dir;
}

void tank::SetSpeed(int sp) {
    data.speed = sp;
}

void tank::SetHealth(int h){
    data.health = h;
}

void tank::SetCenter(QPoint c) {
    data.center = c;
}

void tank::SetType(int t) {
    data.type = t;
}

int tank::GetDirection() {
    return data.direction;
}

int tank::GetSpeed(){
    return data.speed;
}

int tank::GetHealth() {
    return data.health;
}

QPoint tank::GEtCenter() {
    return data.center;
}

int tank::GetType() {
    return data.type;
}


tank_mes tank::GetData() {
    return data;
}

QRect tank::GetBounds() {
    return QRect(data.center.x() - TANKSIZE/2 , data.center.y() -TANKSIZE/2 , TANKSIZE,TANKSIZE);
}

bool checkMoveUPBullet(int x,int y, mymap * checkmap) {
    int _x = x;
    int _y = y;
    ///    qDebug() << y;
    x = x / TILESIZE;
    y = y / TILESIZE;

    if ( _y % TILESIZE == 0)
        if (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)
            return false;
    x = (_x - (TANKSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
        return false;

    x = (_x + (TANKSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
        return false;
    return true;
}

bool checkMoveDownBullet(int x,int y, mymap * checkmap) {

    int _x = x;
    int _y = y;
    //    qDebug() << y;
    //int x2 = x ;
    y = y / TILESIZE;
    x = x / TILESIZE;
    if ( _y % TILESIZE == 0)
        if (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)
            return false;
    x = (_x - (TANKSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
        return false;

    x = (_x + (TANKSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
        return false;
    return true;

}

bool checkMoveRightBullet(int x,int y, mymap * checkmap) {
    int _x = x;
    int _y = y;
    //    qDebug() << x;
    x = x / TILESIZE;
    y= y / TILESIZE;

    if ( _x % TILESIZE == 0)
        if (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)
            return false;

    y = (_y - (TANKSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
        return false;

    y = (_y + (TANKSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
        return false;
    return true;
}

bool checkMoveLeftBullet(int x,int y, mymap * checkmap) {
    int _x = x;
    int _y = y;
    //    qDebug() << x;
    x = x / TILESIZE;
    y= y / TILESIZE;

    if ( _x % TILESIZE == 0)
        if (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)
            return false;


    y = (_y - (TANKSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
        // if ( (_y < (y * TILESIZE)) )
        return false;

    y = (_y + (TANKSIZE/2)) / TILESIZE;
    if (  (checkmap->area[x][y] < 3 || checkmap->area[x][y] > 6)   )
        // if ( (_y < (y * TILESIZE)) )
        return false;
    return true;
}

void tank::move(int dir, mymap * checkmap) {
    if ( dir > 0)
        data.direction = dir;
    switch(dir){
    case 1: {
        int temp = data.center.y()- data.speed ;
        //        qDebug() << temp << temp / TILESIZE << (temp / TILESIZE)*TILESIZE<< checkMoveUP(data.center.x(),temp- (TANKSIZE/2),checkmap);
        if (temp > 0 && checkMoveUPBullet(data.center.x(),(temp-(TANKSIZE/2)),checkmap))
            data.center.setY(temp);
    }break;
    case 2: {

        int temp =data.center.x()+ data.speed ;
        //       qDebug() << temp << temp / TILESIZE << (temp / TILESIZE)*TILESIZE<< checkMoveRight(temp+ (TANKSIZE/2),data.center.y(),checkmap);
        if (temp < 640 && checkMoveRightBullet(temp+ (TANKSIZE/2),data.center.y(),checkmap))
            data.center.setX(temp);
    }break;
    case 3: {

        int temp = data.center.y()+data.speed;
        //        qDebug() << temp << temp / TILESIZE << (temp / TILESIZE)*TILESIZE<< checkMoveDown(data.center.x(),temp + (TANKSIZE/2),checkmap);
        if (temp < 640 && checkMoveDownBullet(data.center.x(),temp + (TANKSIZE/2),checkmap))
            data.center.setY(temp);
    }break;
    case 4: {

        int temp = data.center.x()-data.speed;
        //        qDebug() << temp << temp / TILESIZE << (temp / TILESIZE)*TILESIZE<<  checkMoveLeft(temp - (TANKSIZE/2),data.center.y(),checkmap);
        if ( temp > 0 && checkMoveLeftBullet(temp-(TANKSIZE/2) ,data.center.y(),checkmap))
            data.center.setX(temp);
    }break;
    }
}


bullet* tank::shoot() {
    bullet *ball = new bullet();
    ball->SetDir(data.direction);
    switch(data.direction){
    case 1: {
        ball->SetCenter(data.center-QPoint(0,TANKSIZE/2+2*BALLSIZE));
    }break;
    case 2: {
        ball->SetCenter(data.center+QPoint(TANKSIZE/2+2*BALLSIZE,0));
    }break;
    case 3: {
        ball->SetCenter(data.center+QPoint(0,TANKSIZE/2+2*BALLSIZE));
    }break;
    case 4: {
        ball->SetCenter(data.center-QPoint(TANKSIZE/2+2*BALLSIZE,0));
    }
    }

    return ball;
}




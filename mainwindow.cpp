#include "mainwindow.h"
#include <QAction>
#include <QObject>
#include <QKeyEvent>
#include "tank.h"
#include "tank_player.h"
#include <iostream>
#include "controlwidget.h"
#include <QDebug>
#include <QSound>
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
  //   ui(new Ui::MainWindow)
{
    // Initializating Widget and elements
    // поубирать в конце
    players = new QVector<tank_player*>;
    enemies = new QVector<tank*>;
    bullets = new QVector<bullet*>;
    explosions = new QVector<explosion*>;
    timer = new QTimer();
    counter = 0;
    counter_sec = 0;
    makeGui();
    retranslateUi();
    connectsignals();
    QMetaObject::connectSlotsByName(this);



    // TODO loadsettings!

    mode = 1 ; // client
    Host = "127.0.0.1";
    Port = 30000;
    timer->setInterval(1000);
    changemode();
    gameisstart = false;
    loadmaps();
    loadsound();

    /*  TODO

        LOOP!

      */
    emit gamestart(false);
}



void MainWindow::connectsignals() {
    connect(actionNew,SIGNAL(triggered(bool)),this,SLOT(newgame()));
    connect(actionClient,SIGNAL(triggered(bool)),this,SLOT(changemodetoClient()));
    connect(actionEditor,SIGNAL(triggered(bool)),this,SLOT(changemodetoEditor()));

    connect(this,SIGNAL(changedmode(int)),DrawArea,SLOT(areachangedmode(int)));
    connect(this,SIGNAL(changedmode(int)),InfoArea,SLOT(areachangedmode(int)));
    connect(this,SIGNAL(gamestart(bool)),DrawArea,SLOT(gamestart(bool)));

    connect(timer,SIGNAL(timeout()),this,SLOT(gamerunning()));
    connect(this,SIGNAL(soundplay(int)),this,SLOT(soundplayon(int)));

    connect(this->InfoArea,SIGNAL(numblock(int)),this->DrawArea,SLOT(getblock(int)));

    connect(this->menuSetting,SIGNAL(triggered(QAction*)),this,SLOT(setting()));

    connect(this,SIGNAL(gameonpause(bool)),this->DrawArea,SLOT(gameispauseslot(bool)));
}


//SLOTS

void MainWindow::cleanmas() {
    num_players = 2;
    for(quint16 i = 0; i < players->size(); i++) {
        delete players->at(i);
    }
    players->clear();
    for(quint16 i = 0; i < bullets->size(); i++) {
        delete bullets->at(i);
    }
    bullets->clear();
    for(quint16 i = 0; i < controlplayer.size(); i++) {
        delete controlplayer.at(i);
    }
}

void MainWindow::newgame() {

    if (mode == 1) {
        cleanmas();

        num_players = 2;
        for(quint8 i = 0; i < num_players; i++) {
            players->push_back(new tank_player());
            controlplayer.push_back(new player());

        }

        for(quint8 i = 0; i < num_enemy; i++) {
            enemies->push_back(botan.create());
        }

        players->at(0)->SetCenter(maps.at(0)->base_pos-QPoint(50,0));
        if (num_players > 1)
            players->at(1)->SetCenter(maps.at(0)->base_pos+QPoint(100,0));

        gamerisplayed = false;

        /*
        _client = new clienttank(Host,Port,this);
        _client->setPlayers(players);
        _client->connecttoHost();
*/

        level = 9;
        num_enemy = level *3 + 15;
        timelevel = 200;
        DrawArea->setBullets(bullets);
        DrawArea->setPlayers(players);
        DrawArea->setEnemy(enemies);
        DrawArea->setmap(maps.at(level));
        DrawArea->setExplosions(explosions);
        gameisstart = true;
        emit gamestart(gameisstart);
        timer->start();
    }
}

void MainWindow::changemodetoClient() {
    mode = 1;
    changemode();
}

void MainWindow::changemodetoServer() {
    mode = 0;
    changemode();
}

void MainWindow::changemodetoEditor() {
    mode = 2;
    changemode();
}

void MainWindow::setting() {
    QDialog d;
    d.show();
}

inline int GetDirection(int k,int &nplayer) {
    int player_direction = -1;
    switch( k ) {
    case ((int)Qt::Key_W): {
        player_direction = 1;
        nplayer = 0;
    }break;
    case Qt::Key_A: {
        player_direction = 4;
        nplayer = 0;
    }break;
    case Qt::Key_S: {
        player_direction = 3;
        nplayer = 0;
    }break;
    case Qt::Key_D: {
        player_direction = 2;
        nplayer = 0;
    }break;
    case Qt::Key_Space: {
        player_direction = -2;
        nplayer = 0;
    }break;

    case Qt::Key_Up: {
        player_direction = 1;
        nplayer = 1;
    }break;
    case Qt::Key_Left: {
        player_direction = 4;
        nplayer = 1;
    }break;
    case Qt::Key_Down: {
        player_direction = 3;
        nplayer = 1;
    }break;
    case Qt::Key_Right: {
        player_direction = 2;
        nplayer = 1;
    }break;
    case Qt::Key_Enter: {
        player_direction = -2;
        nplayer = 1;
    }break;
    }
    return player_direction;
}


void MainWindow::checkbullet() {
    for (quint16 i = 0; i < bullets->size(); i++)
    {
        if(!bullets->at(i)->move(maps.at(level)))
        {
            explosions->push_back(bullets->at(i)->getExpl());
            bullets->erase(bullets->begin()+i);
        }
    }
/*
    for (quint16 i = 0; i < bullets->size(); i++)
    {
        QRect r = bullets->at(i)->GetBounds();
        for (quint16 j = 0; j < bullets->size(); j++)
        {
            if ( i != j)
            {
                QRect b = bullets->at(i)->GetBounds();
                if(r.intersects(b)){
                    delete bullets->at(i);
                    bullets->erase(bullets->begin()+i);
                    delete bullets->at(j);
                    bullets->erase(bullets->begin()+j);
                }
            }
        }
    }
*/
    for (quint16 i = 0; i < bullets->size(); i++)
    {
        QRect  r = bullets->at(i)->GetBounds();
        for (quint16 j = 0; j < players->size(); j++)
        {
            if (!players->at(i)->isdead) {
                QRect p = players->at(j)->GetBounds();
                if(r.intersects(p)){
                    players->at(j)->SetHealth(players->at(j)->GetHealth()-60);
                    if (players->at(j)->GetHealth() <= 0)
                        players->at(j)->isdead = true;
                    explosions->push_back(bullets->at(i)->getExpl());
                    delete bullets->at(i);
                    bullets->erase(bullets->begin()+i);
                }
            }
        }
    }
}

void MainWindow::checkplayers() {

    for (quint8 i = 0; i < num_players; i++)
    {
        if (!players->at(i)->isdead)
            controlplayer.at(i)->shoot++;
        if (controlplayer.at(i)->player_direction != -1) {
            if (controlplayer.at(i)->player_direction == -2 && controlplayer.at(i)->shoot > 20)
            {
                emit soundplay(0);
                bullets->push_back(players->at(i)->shoot());
                qDebug() << "shoot";
                controlplayer.at(i)->shoot = 0;
            }
            else
                players->at(i)->move(controlplayer.at(i)->player_direction,maps.at(level));
        }
    }

}

void MainWindow::gamerunning() {
    if (gameisstart) {
        if(num_players == 0)
        {
            gameisstart = false;
            game_win = false;
            gamerisplayed = true;
        }
        if(num_enemy == 0)
        {
            gameisstart = false;
            game_win = true;
            gamerisplayed = true;
        }

        counter++;
        if ( counter == 50)
        {
            counter_sec++;
            timelevel--;
            emit taimerinfo(timelevel);
        }
        if(timelevel == 0) {
            gameisstart = false;
            game_win = true;
            gamerisplayed = true;
        }
        checkbullet();
        checkplayers();
        botan.analyze(maps.at(level),players);
        botan.command(enemies);
        DrawArea->update();
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    if ( mode == 1) {
        if (event->key() == 0x01000008 || event->key() == 0x50) {
            if (!gameispause) {
                timer->stop();
                gameisstart = false;
                gameispause = true;
                emit gameonpause(gameispause);
            } else
            {
                timer->start();
                gameisstart = true;
                gameispause = false;
                emit gameonpause(gameispause);
            }

        }
        if (gameisstart)
        {
            int dir;
            int nplayer;
            dir = GetDirection(event->key(),nplayer);
            if (dir != -1)
                controlplayer.at(nplayer)->player_direction = dir;
        }
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event) {
    if (gameisstart && mode == 1 && (!gameispause)) {
        for (quint8 i = 0; i < num_players; i++)
        {
            int dir;
            int nplayer;
            dir = GetDirection(event->key(),nplayer);
            if (dir == controlplayer.at(nplayer)->player_direction )
                controlplayer.at(nplayer)->player_direction = -1;
        }
    }
}

void MainWindow::changemode() {
    switch(mode) {
    case 0: { // server
        actionClient->setChecked(false);
        actionEditor->setChecked(false);
        //        actionServer->setChecked(true);
//        actionLoadLevel->setEnabled(false);
        actionSaveLevel->setEnabled(false);
        actionPause->setEnabled(false);

        //        _serv = new servertank(Port);


    }break;
    case 1: { // client
        actionClient->setChecked(true);
        actionEditor->setChecked(false);
        //        actionServer->setChecked(false);
//        actionLoadLevel->setEnabled(false);
        actionSaveLevel->setEnabled(false);
        actionPause->setEnabled(true);


    }break;
    case 2: { // editor
        timer->stop();
        gameisstart = false;
        actionClient->setChecked(false);
        actionEditor->setChecked(true);
        //        actionServer->setChecked(false);
//        actionLoadLevel->setEnabled(true);
        actionSaveLevel->setEnabled(true);
        actionPause->setEnabled(false);


        gameispause = true;
        emit gameonpause(gameispause);
        /*editor = new map;
        DrawArea->setmap(editor);*/
    }break;
    }
    emit changedmode(mode);
}

void MainWindow::makeGui() {
    if (this->objectName().isEmpty())
        this->setObjectName(QString::fromUtf8("this"));
    this->resize(830, 700);
    this->setMinimumSize(QSize(830, 700));
    this->setMaximumSize(QSize(830, 700));
    actionNew = new QAction(this);
    actionNew->setObjectName(QString::fromUtf8("actionNew"));

//    actionLoadLevel = new QAction(this);
//    actionLoadLevel->setObjectName(QString::fromUtf8("actionNew"));

    actionSaveLevel = new QAction(this);
    actionSaveLevel->setObjectName(QString::fromUtf8("actionNew"));

    actionExit = new QAction(this);
    actionExit->setObjectName(QString::fromUtf8("actionExit"));
    actionPause = new QAction(this);
    actionPause->setObjectName(QString::fromUtf8("actionPause"));
    actionClient = new QAction(this);
    actionClient->setObjectName(QString::fromUtf8("actionClient"));
    actionClient->setCheckable(true);
    //    actionServer = new QAction(this);
    //    actionServer->setObjectName(QString::fromUtf8("actionServer"));
    //    actionServer->setCheckable(true);
    actionEditor = new QAction(this);
    actionEditor->setObjectName(QString::fromUtf8("actionEditor"));
    actionEditor->setCheckable(true);
    centralWidget = new QWidget(this);
    centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
    horizontalLayoutWidget = new QWidget(centralWidget);
    horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
    horizontalLayoutWidget->setGeometry(QRect(40, 20, 720, 640));
    horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
    horizontalLayout->setSpacing(6);
    horizontalLayout->setContentsMargins(0, 0, 0, 0);
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    horizontalLayout->setContentsMargins(0, 0, 0, 0);
    DrawArea = new DrawWidget(horizontalLayoutWidget);

    horizontalLayout->addWidget(DrawArea);

    InfoArea = new InfoWidget(horizontalLayoutWidget);
    InfoArea->setObjectName(QString::fromUtf8("InfoArea"));
    InfoArea->setMinimumSize(QSize(100, 640));
    InfoArea->setMaximumSize(QSize(100, 640));

    horizontalLayout->addWidget(InfoArea);

    this->setCentralWidget(centralWidget);
    menuBar = new QMenuBar(this);
    menuBar->setObjectName(QString::fromUtf8("menuBar"));
    menuBar->setGeometry(QRect(0, 0, 820, 21));
    menuExit = new QMenu(menuBar);
    menuExit->setObjectName(QString::fromUtf8("menuExit"));
    menuMode = new QMenu(menuBar);
    menuMode->setObjectName(QString::fromUtf8("menuMode"));
    menuSetting = new QMenu(menuBar);
    menuSetting->setObjectName(QString::fromUtf8("menuSetting"));
    menuHelp = new QMenu(menuBar);
    menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
    this->setMenuBar(menuBar);
    mainToolBar = new QToolBar(this);
    mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
    this->addToolBar(Qt::TopToolBarArea, mainToolBar);

    menuBar->addAction(menuExit->menuAction());
    menuBar->addAction(menuMode->menuAction());
    menuBar->addAction(menuSetting->menuAction());
    menuBar->addAction(menuHelp->menuAction());
    menuExit->addAction(actionNew);
    menuExit->addSeparator();
//    menuExit->addAction(actionLoadLevel);
    menuExit->addAction(actionSaveLevel);
    menuExit->addAction(actionPause);
    menuExit->addAction(actionExit);
    menuMode->addAction(actionClient);
    //    menuMode->addAction(actionServer);
    menuMode->addAction(actionEditor);
    mainToolBar->addSeparator();
}

void MainWindow::retranslateUi()
{
    this->setWindowTitle(QApplication::translate("this", "Battle Tanks", 0, QApplication::UnicodeUTF8));
    actionNew->setText(QApplication::translate("this", "New", 0, QApplication::UnicodeUTF8));
//    actionLoadLevel->setText(QApplication::translate("this", "Save Level", 0, QApplication::UnicodeUTF8));
//    actionSaveLevel->setText(QApplication::translate("this", "Load Level", 0, QApplication::UnicodeUTF8));
    actionExit->setText(QApplication::translate("this", "Exit", 0, QApplication::UnicodeUTF8));
    actionPause->setText(QApplication::translate("this", "Pause", 0, QApplication::UnicodeUTF8));
    actionClient->setText(QApplication::translate("this", "Client", 0, QApplication::UnicodeUTF8));
    //    actionServer->setText(QApplication::translate("this", "Server", 0, QApplication::UnicodeUTF8));
    actionEditor->setText(QApplication::translate("this", "Editor", 0, QApplication::UnicodeUTF8));
    menuExit->setTitle(QApplication::translate("this", "Menu", 0, QApplication::UnicodeUTF8));
    menuMode->setTitle(QApplication::translate("this", "Mode", 0, QApplication::UnicodeUTF8));
    menuSetting->setTitle(QApplication::translate("this", "Settings", 0, QApplication::UnicodeUTF8));
    menuHelp->setTitle(QApplication::translate("this", "Help", 0, QApplication::UnicodeUTF8));
} // retranslateUi

MainWindow::~MainWindow()
{

    /* delete actionNew;
    delete actionExit;
    delete actionPause;
    delete actionClient;
    delete actionServer;
    delete actionEditor;
    delete centralWidget;
    delete horizontalLayoutWidget;
    delete horizontalLayout;*/
    //   delete DrawArea;
    //   delete InfoArea;
    /*   delete menuBar;

    delete menuExit;
    delete menuMode;
    delete menuSetting;
    delete menuHelp;
    delete mainToolBar;*/
    cleanmas();

    delete players;
    delete bullets;
    if (editor)
        delete editor;

    delete players;
    delete timer;
    controlplayer.clear();
    for(quint16 i = 0; i < maps.size(); i++) {
        delete maps.at(i);
    }

    //    delete _serv;
    //delete _client;
}

void MainWindow::loadsound() {
    //soundshoot = new QSound("qrc:///sound/sound/bc_shoot.wav");

}

void MainWindow::soundplayon(int n) {
    switch(n) {
    case 0: {
    //    soundshoot->play();
        break;
    }
    }
}

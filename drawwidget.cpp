#include <iostream>
#include <QBrush>
#include <QVector>
#include <QMouseEvent>

#include "drawwidget.h"
#include "controlwidget.h"

using namespace std;


DrawWidget::DrawWidget(QWidget *parent) :
    QWidget(parent)
{
    setObjectName(QString::fromUtf8("DrawArea"));
    setMinimumSize(QSize(AREAPIXELS, AREAPIXELS));
    setMaximumSize(QSize(AREAPIXELS, AREAPIXELS));
    loadlevels();
    loadimages();
    notdrawed = true;
    drawblock = false;
    gameispause = false;
    update();
}

void DrawWidget::paintEvent(QPaintEvent *) {
    QPainter painter(this);

    QBrush b;
    b.setColor(Qt::black);

    painter.fillRect(0,0,640,640,QColor(0,0,0,255));
    /*if (notdrawed) {
        drawarea(painter);
        notdrawed = false;
    }*/
    /*
    for(quint8 i = 0; i < 4; i++) {
        for(quint8 j = 0; j < 4; j++) {
            QRectF target1(i*40+5, j*40+5, 40, 40);
            painter.drawImage(target1, players_image[i][j]);
        }
    }
*/

    if ( mode == 2) { /// editor
        painter.fillRect(0,0,640,640,QColor(50,50,50,255));
        if (drawblock) {
            drawblock = false;
            editor[drawx][drawy] = typedrawblock;
            drawareablocks(painter);
        }

    } else
        if ( mode == 1 && gameisstart ) {

            if (gameispause) {
                QFont font = painter.font();
                font.setWeight(80);
                QPen pen;
                pen.setColor(Qt::red);
                pen.setWidthF(10);
                painter.setPen(pen);
                painter.setFont(font);
                QRect rect(320-60,320,80,80);
                painter.drawText(rect, Qt::AlignCenter, tr("PAUSE"));
            } else {
                drawarea(painter);
                drawplayers(painter);
                drawbullets(painter);
                //drawtrees(painter);
                //drawexplosion(painter);
            }
        }


}


void DrawWidget::mousePressEvent(QMouseEvent *event) {
    drawx = event->x() / (TILESIZE*2);
    drawy = event->y() / (TILESIZE*2);

    /// DEBUG
    /// qDebug() << drawx << drawy << typedrawblock <<  event->x() << event->y();


    drawblock = true;
    update();
}

inline void DrawWidget::drawplayers(QPainter &painter) {
    for ( quint8 i = 0; i < players->size(); i++) {
        if (!players->at(i)->isdead) {
            QRect rect = players->at(i)->GetBounds();
            QImage &im = (players_image[players->at(i)->GetType()-1][players->at(i)->GetDirection()-1]);
            painter.drawImage(rect, im);
        }
    }
}


inline void DrawWidget::drawenemies(QPainter &painter) {
    for ( quint8 i = 0; i < enemies->size(); i++) {
        if (!enemies->at(i)->isdead) {
            QRect rect = enemies->at(i)->GetBounds();
            QImage &im = (enemy_image[players->at(i)->GetType()-1][enemies->at(i)->GetDirection()-1]);
            painter.drawImage(rect, im);
        }
    }
}


void DrawWidget::drawareablocks(QPainter &painter) {
    for(quint32 i = 0; i < AREASIZE/2; i++) {
        for(quint32 j = 0; j < AREASIZE/2; j++) {
            QRectF target1(i*TILESIZE*2, j*TILESIZE*2, TILESIZE*2, TILESIZE*2);
            painter.drawImage(target1, blocks[editor[i][j]]);
        }
    }
}

void DrawWidget::drawarea(QPainter &painter) {
    for(quint32 i = 0; i < AREASIZE; i++) {
        for(quint32 j = 0; j < AREASIZE; j++) {
            QRectF target1(i*TILESIZE, j*TILESIZE, TILESIZE, TILESIZE);
            painter.drawImage(target1, tiles[drawmap->area[i][j]]);
        }
    }

    QRectF target2(drawmap->base_pos.x(), drawmap->base_pos.y(), TILESIZE*2, TILESIZE*2);
    painter.drawImage(target2,blocks[16]);
}

void DrawWidget::drawbullets(QPainter &painter) {
    for(int i = 0; i < bullets->size(); i++) {
        /*     QRectF target2;
        if ((bullets->at(i)->GetDir()-1) == 0)
            target2 = QRectF(bullets->at(i)->GetCenter()-QPoint(BALLSIZE/2,BALLSIZE/2), QPoint(BALLSIZE,BALLSIZE*2));
        else
            if ((bullets->at(i)->GetDir()-1) == 1)
                target2 = QRectF(bullets->at(i)->GetCenter()-QPoint(3/2*BALLSIZE,BALLSIZE/2), QPoint(BALLSIZE*2,BALLSIZE));

            else
                if ((bullets->at(i)->GetDir()-1) == 2)
                    target2 = QRectF(bullets->at(i)->GetCenter()-QPoint(BALLSIZE/2,3/2*BALLSIZE), QPoint(BALLSIZE,BALLSIZE*2));
                else
                    if ((bullets->at(i)->GetDir()-1) == 3)
                        target2 = QRectF(bullets->at(i)->GetCenter()-QPoint(BALLSIZE/2,BALLSIZE/2), QPoint(BALLSIZE*2,BALLSIZE));
*/
        QRectF target1(bullets->at(i)->GetCenter()-QPoint(BALLSIZE/2,BALLSIZE/2), QPoint(BALLSIZE,BALLSIZE));
        //painter.drawImage(target2,comet[bullets->at(i)->GetDir()-1]);
        painter.drawImage(target1,ball);
    }
}

void DrawWidget::drawtrees(QPainter &painter) {
    for(quint32 i = 0; i < AREASIZE; i++) {
        for(quint32 j = 0; j < AREASIZE; j++) {
            if (drawmap->area[i][j] == BLOCK_TREE) {
                QRectF target1(i*TILESIZE, j*TILESIZE, TILESIZE, TILESIZE);
                painter.drawImage(target1, tiles[drawmap->area[i][j]]);
            }
        }
    }
}

void DrawWidget::drawexplosion(QPainter &painter) {
    for(int i = 0; i < expl->size(); i++) {
        QRectF target1(expl->at(i)->GetCenter()-QPoint(TILESIZE,TILESIZE),QPoint(TILESIZE,TILESIZE));
        painter.drawImage(target1, explosions[expl->at(i)->GetTime()]);
        expl->at(i)->SetTime(expl->at(i)->GetTime()+1);
        if (expl->at(i)->GetTime() == EXPLCOUNT-1)
            expl->erase(expl->begin()+i);

    }
}

void DrawWidget::getblock(int i) {
    typedrawblock = i;
}

void DrawWidget::gameispauseslot(bool b) {
    gameispause = b;
    update();
}

void DrawWidget::loadlevels(/*QString file*/) {
    /* memset(*(area+4)+5,1,20);
    memset(*(area+5)+5,1,20);*/
}

void DrawWidget::setPlayers(QVector<tank_player*> *t) {
    players = t;
}

void DrawWidget::setBullets(QVector<bullet*> *b) {
    bullets = b;
}

void DrawWidget::setExplosions(QVector<explosion*> *b) {
    expl = b;
}

void DrawWidget::setEnemy(QVector<tank*> *b) {
    enemies = b;
}


void DrawWidget::loadimages() {

    QString str;
    for (quint8 i = 0; i < IMAGECOUNT; i++) {
        str = TILESPATH + QString("%1").arg(i)+".png";
        cout << tiles[i].load(str.toStdString().c_str(),"PNG");
    }

    for (quint8 i = 0; i < BLOCKSCOUNT; i++) {
        str = BLOCKSPATH + QString("%1").arg(i)+".png";
        cout << blocks[i].load(str.toStdString().c_str(),"PNG");
    }

    for (quint8 i = 0; i < EXPLCOUNT; i++) {
        str = EXPLPATH + QString("%1").arg(i+1)+".png";
        explosions[i].load(str.toStdString().c_str(),"PNG");
    }

    str = BULLETPATH +QString(".png");
    ball.load(str.toStdString().c_str(),"PNG");

    for (quint8 i = 0; i < 4; i++) {
        str = COMETPATH + QString("%1").arg(i+1)+".png";
        comet[i].load(str.toStdString().c_str(),"PNG");
    }

    QTransform myTransform;
    myTransform.rotate(90);
    for (quint8 i = 0; i < 4; i++) {
        str = TANKSSPATH + QString("%1").arg(i+1)+".png";
        players_image[i][0].load(str.toStdString().c_str(),"PNG");
        players_image[i][1] = players_image[i][0].transformed(myTransform);
        players_image[i][2] = players_image[i][0].mirrored(false,true);
        players_image[i][3] = players_image[i][1].mirrored(true,false);
    }

    for (quint8 i = 0; i < 4; i++) {
        str = ENEMYSSPATH + QString("%1").arg(i+1)+".png";
        enemy_image[i][0].load(str.toStdString().c_str(),"PNG");
        enemy_image[i][1] = enemy_image[i][0].transformed(myTransform);
        enemy_image[i][2] = enemy_image[i][0].mirrored(false,true);
        enemy_image[i][3] = enemy_image[i][1].mirrored(true,false);
    }
}
// SLOTS
void DrawWidget::areachangedmode(int _m) {
    mode = _m;
    if ( mode == 2)
    {
        memset(&editor,15,sizeof(quint8)*AREASIZE*AREASIZE/4);
    }
    update();
}

void DrawWidget::gamestart(bool s) {
    gameisstart = s;
    update();
}

void DrawWidget::setmap(mymap *draw) {

    drawmap = draw;
}

/* void GetTiles() {
     Saving Tiles

    QImage blocks[16];
    QString str;
    for (quint8 i = 0; i < 16; i++) {
        str = ":/block/images/blocks/block_" + QString("%1").arg(i)+".png";
        blocks[i].load(str.toStdString().c_str(),"PNG");
    }
    tiles[0] = blocks[0].copy(0,0,32,32);
    tiles[1] = blocks[2].copy(0,0,32,32);
    tiles[2] = blocks[7].copy(0,0,32,32);
    tiles[3] = blocks[10].copy(0,0,32,32);
    tiles[4] = blocks[11].copy(0,0,32,32);
    tiles[5] = blocks[12].copy(0,0,32,32);

    for (quint8 i = 0; i < 6; i++) {
        str = "/home/dimon/Documents/tankstest/images/tiles/tile"+QString("%1").arg(i)+".png";
        cout << tiles[i].save(str,"PNG") ;
    }
    str = ":/1/minitiles.png";
    QImage minitails(str.toStdString().c_str(),"PNG");
    // /   QImage b2[15];
    for (quint8 i = 0; i < 4; i++) {
        for (quint8 j = 0; j < 4; j++) {
            tiles[6+(i*4+j)] = minitails.copy(8+j*(64+16),8+i*(64+16),64,64);
        }
    }

    //QString str;
    for (quint8 i = 6; i < 22; i++) {
        str = "/home/dimon/Documents/tankstest/images/tiles/tile"+QString("%1").arg(i)+".png";
        tiles[i] = tiles[i].scaled(32,32);
        cout << tiles[i].save(str,"PNG");
    }

}
 */
